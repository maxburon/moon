const svg = document.querySelector('svg')
const body = document.querySelector('body')
const panelContent = document.querySelector('#info .content')
const panel = document.querySelector('#info')

function getAreaId(eltId) {
  return 'area-' + eltId
}

function insertElementMap(elt) {
  const link = document.createElementNS('http://www.w3.org/2000/svg', 'a')
  link.setAttributeNS(null, 'href', '#' + elt.id)
  link.setAttributeNS(null, 'title', elt.title)
  
  const rect = document.createElementNS('http://www.w3.org/2000/svg', 'rect')
  rect.id = getAreaId(elt.id)
  rect.setAttributeNS(null, 'x', elt.area.x)
  rect.setAttributeNS(null, 'y', elt.area.y)
  rect.setAttributeNS(null, 'width', elt.area.width)
  rect.setAttributeNS(null, 'height', elt.area.height)

  const title = document.createElementNS('http://www.w3.org/2000/svg', 'title')
  const titleTxt = document.createTextNode(elt.title)

  title.appendChild(titleTxt)
  rect.appendChild(title)

  const circ = document.createElementNS('http://www.w3.org/2000/svg', 'circle')
  circ.classList.add('pointer')
  circ.setAttributeNS(null, 'cx', elt.area.x + elt.area.width / 2)
  circ.setAttributeNS(null, 'cy', elt.area.y + elt.area.height / 2)
  circ.setAttributeNS(null, 'r', 5)

  link.appendChild(rect)
  link.appendChild(circ)
  svg.appendChild(link)
}

function highlightElt(elt) {
  const area = svg.getElementById(getAreaId(elt.id))
  area.classList.add('highlight')
}

function displayElt (elt) {
  closeElts()
  openPanel()

  const title = document.createElement('h2')
  title.innerText = elt.title
  panelContent.appendChild(title)

  if (elt.diametre) {
    const dia = createValueUncertainty(elt.diametre, "diamètre")
    panelContent.appendChild(dia)
  }

  highlightElt(elt)
}

function createValueUncertainty (v, title) {
  const p = document.createElement('p')
  p.classList.add('field')

  const t = document.createElement('span')
  t.classList.add('name')
  t.innerText = title
  p.appendChild(t)
  const value = document.createElement('span')
  value.classList.add('value')
  value.innerText = v.value
  p.appendChild(value)
  const unc = document.createElement('span')
  unc.classList.add('uncertainty')
  unc.innerText = v.uncertainty
  p.appendChild(unc)

  return p
}

function closeElts () {
  clearPanel()
  const rectangles = svg.querySelectorAll('rect')
  for (let area of rectangles) {
    area.classList.remove('highlight')
  }
}

function openPanel () {
  body.classList.remove('closed')
}

function closePanel () {
  body.classList.add('closed')
  closeElts()
}

function clearPanel () {
  while (panelContent.firstElementChild) {
    panelContent.firstElementChild.remove()
  }
}

fetch('data.json')
  .then(res => res.json())
  .then(elts => {
    for (let elt of elts) {
      if (elt.area) {
        insertElementMap(elt)
      }
    }

    function displayHanchor(hash) {
      let elt = elts.filter(e => e.id === hash.substring(1))[0]
      if (elt) {
        displayElt(elt)
      } else {
        closePanel()
      }
    }

    window.addEventListener('hashchange', event => displayHanchor(location.hash))

    displayHanchor(location.hash)
  })
